#!/usr/bin/env bash

for f in $(ls *.wav); do \
	ffmpeg -i $f -acodec mp3 "$(basename "$f" .wav).mp3"; \
done
