import sys
import SDT
import numpy as np
from scipy.io import wavfile
import os
from itertools import chain, repeat


def normalize(x):
	m, M = np.min(x), np.max(x)
	return (x - m)/(M - m)


if __name__ == "__main__":
	fs = 44100
	duration = 5
	ofname = "out.wav"
	pfname = "parameters.npy"
	plot_ = True if len(sys.argv) < 2 else eval(sys.argv[1])
	double_hit = 0.025
	
	SDT.setSampleRate(fs)
	length = int(duration*fs)
	nhit = int(double_hit*fs)
	
	# Build structures
	hammer = SDT.Inertial(mass=0.01)
	resonator = SDT.Resonator.fromList(*np.load(pfname), 1)
	imp = SDT.Impact(Stiffness=1e+7, Dissipation=0.1, Shape=0.1, FirstPoint=0, SecondPoint=0, FirstResonator=hammer, SecondResonator=resonator)
	
	# Synthesize audio
	tmp = np.zeros(2).astype(np.double)
	V = chain([-1], repeat(0, nhit - 1), [-1], repeat(0, length - 1 - nhit))
	out = np.array([tmp[1] for i, v in enumerate(V) if imp.dsp(0, v, 0, 0, 0, 0, tmp) is None])

	# Plot and save
	if plot_:
		import matplotlib.pyplot as plt
		plt.plot(np.arange(length)/fs, out)
		plt.ion(); plt.draw(); plt.pause(.001)
	wavfile.write(ofname, fs, normalize(out).astype(np.float32))
	if plot_:
		os.system("aplay {}".format(ofname))
