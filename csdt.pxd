#include "../SDT/src/SDT/SDTCommon.h"
#include "../SDT/src/SDT/SDTResonators.h"
#include "../SDT/src/SDT/SDTInteractors.h"

cdef extern from "SDT/src/SDT/SDTCommon.h":
	void SDT_setSampleRate(double sampleRate)

cdef extern from "SDT/src/SDT/SDTResonators.h":
	ctypedef struct SDTResonator:
		pass
	
	SDTResonator* SDTResonator_new(unsigned int nModes, unsigned int nPickups)
	void SDTResonator_free(SDTResonator *x)
	
	double SDTResonator_getPosition(SDTResonator *x, unsigned int pickup)
	double SDTResonator_getVelocity(SDTResonator *x, unsigned int pickup)
	int SDTResonator_getNPickups(SDTResonator *x)
	
	void SDTResonator_setPosition(SDTResonator *x, unsigned int pickup, double f)
	void SDTResonator_setVelocity(SDTResonator *x, unsigned int pickup, double f)
	void SDTResonator_setFrequency(SDTResonator *x, unsigned int mode, double f)
	void SDTResonator_setDecay(SDTResonator *x, unsigned int mode, double f)
	void SDTResonator_setWeight(SDTResonator *x, unsigned int mode, double f)
	void SDTResonator_setGain(SDTResonator *x, unsigned int pickup, unsigned int mode, double f)
	void SDTResonator_setFragmentSize(SDTResonator *x, double f)
	void SDTResonator_setActiveModes(SDTResonator *x, unsigned int i)
	
	void SDTResonator_applyForce(SDTResonator *x, unsigned int pickup, double f)
	double SDTResonator_computeEnergy(SDTResonator *x, unsigned int pickup, double f)
	void SDTResonator_dsp(SDTResonator *x)

cdef extern from "SDT/src/SDT/SDTInteractors.h":
	ctypedef struct SDTInteractor:
		pass
	
	void SDTInteractor_setFirstResonator(SDTInteractor *x, SDTResonator *p)
	void SDTInteractor_setSecondResonator(SDTInteractor *x, SDTResonator *p)
	void SDTInteractor_setFirstPoint(SDTInteractor *x, long l)
	void SDTInteractor_setSecondPoint(SDTInteractor *x, long l)
	
	double SDTInteractor_computeForce(SDTInteractor *x)
	void SDTInteractor_dsp(SDTInteractor *x, double f0, double v0, double s0, double f1, double v1, double s1, double *outs)
	
	ctypedef struct SDTImpact:
		pass
	
	SDTInteractor* SDTImpact_new()
	void SDTImpact_free(SDTInteractor *x)
	
	void SDTImpact_setStiffness(SDTInteractor *x, double f)
	void SDTImpact_setDissipation(SDTInteractor *x, double f)
	void SDTImpact_setShape(SDTInteractor *x, double f)
