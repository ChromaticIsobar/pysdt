#!/usr/bin/env bash

files=$(ls *.mp3)
filesarray=( $files )
n=${#filesarray[@]}

echo "<!DOCTYPE html><html><body>"
if [ "$n" -eq "1" ]; then
	echo "<script>window.location.href = \""$files"\"</script>"
else
	echo "<ul>"
	for f in $(ls *.wav) ; do \
		echo "<li><a href=\""$f"\"</a></li>"; \
	done
	echo "</ul>"
fi
echo "</body></html>"
