# distutils: sources = SDT/src/SDT/SDTAnalysis.c SDT/src/SDT/SDTDemix.c SDT/src/SDT/SDTInteractors.c SDT/src/SDT/SDTResonators.c SDT/src/SDT/SDTCommon.c SDT/src/SDT/SDTEffects.c SDT/src/SDT/SDTLiquids.c SDT/src/SDT/SDTSolids.c SDT/src/SDT/SDTComplex.c SDT/src/SDT/SDTFFT.c SDT/src/SDT/SDTModalTracker.c SDT/src/SDT/SDTStructs.c SDT/src/SDT/SDTControl.c SDT/src/SDT/SDTFilters.c SDT/src/SDT/SDTMotor.c SDT/src/SDT/SDTDCMotor.c SDT/src/SDT/SDTGases.c SDT/src/SDT/SDTOscillators.c
# distutils: include_dirs = SDT/src/SDT/

cimport csdt
import numpy as np
cimport numpy as np

from collections.abc import Iterable


def setSampleRate(double sampleRate):
	csdt.SDT_setSampleRate(sampleRate)



cdef class Resonator:
	cdef csdt.SDTResonator* _sdt_resonator
	
	def __cinit__(self, unsigned int nModes=1, unsigned int nPickups=1, *args, **kwargs):
		self._sdt_resonator = csdt.SDTResonator_new(nModes, nPickups)
		if self._sdt_resonator is NULL:
			raise MemoryError()
	
	def __dealloc__(self):
		if self._sdt_resonator is not NULL:
			csdt.SDTResonator_free(self._sdt_resonator)

	@staticmethod
	def fromList(freqs, decays, gains, fragment_size, masses=None):
		resonator = Resonator(len(freqs), len(gains[0]) if isinstance(gains[0], Iterable) else 1)
		for m, (f, d) in enumerate(zip(freqs, decays)):
			resonator.setFrequency(m, f)
			resonator.setDecay(m, d)
			for p, g in enumerate(gains[m] if isinstance(gains[m], Iterable) else [gains[m]]):
				resonator.setGain(p, m, g)
			resonator.setWeight(m, 1 if masses is None else masses[m])
		resonator.setFragmentSize(fragment_size)
		resonator.setActiveModes(len(freqs))
		return resonator

	# --- Getters -------------------------------------------------------------
	def getPosition(self, unsigned int pickup):
		return csdt.SDTResonator_getPosition(self._sdt_resonator, pickup)

	def getVelocity(self, unsigned int pickup):
		return csdt.SDTResonator_getVelocity(self._sdt_resonator, pickup)

	def getNPickups(self):
		return csdt.SDTResonator_getNPickups(self._sdt_resonator)
	# -------------------------------------------------------------------------

	# --- Setters -------------------------------------------------------------
	def setPosition(self, unsigned int pickup, double f):
		csdt.SDTResonator_setPosition(self._sdt_resonator, pickup, f)
		return self

	def setVelocity(self, unsigned int pickup, double f):
		csdt.SDTResonator_setVelocity(self._sdt_resonator, pickup, f)
		return self

	def setFrequency(self, unsigned int mode, double f):
		csdt.SDTResonator_setFrequency(self._sdt_resonator, mode, f)
		return self

	def setDecay(self, unsigned int mode, double f):
		csdt.SDTResonator_setDecay(self._sdt_resonator, mode, f)
		return self

	def setWeight(self, unsigned int mode, double f):
		csdt.SDTResonator_setWeight(self._sdt_resonator, mode, f)
		return self

	def setGain(self, unsigned int pickup, unsigned int mode, double f):
		csdt.SDTResonator_setGain(self._sdt_resonator, pickup, mode, f)
		return self

	def setFragmentSize(self, double f):
		csdt.SDTResonator_setFragmentSize(self._sdt_resonator, f)
		return self

	def setActiveModes(self, unsigned int i):
		csdt.SDTResonator_setActiveModes(self._sdt_resonator, i)
		return self
	# -------------------------------------------------------------------------


class Inertial(Resonator):
	def __init__(self, double mass=1, double fragment_size=1):
		self.setWeight(0, mass)
		self.setFragmentSize(fragment_size)
		self.setFrequency(0, 0)
		self.setDecay(0, 0)
		self.setGain(0, 0, 1)
		self.setActiveModes(1)


cdef class Interactor:
	cdef csdt.SDTInteractor* _sdt_interactor

	def setFirstResonator(self, Resonator p):
		csdt.SDTInteractor_setFirstResonator(self._sdt_interactor, p._sdt_resonator)
		return self

	def setSecondResonator(self, Resonator p):
		csdt.SDTInteractor_setSecondResonator(self._sdt_interactor, p._sdt_resonator)
		return self

	def setFirstPoint(self, long l):
		csdt.SDTInteractor_setFirstPoint(self._sdt_interactor, l)
		return self

	def setSecondPoint(self, long l):
		csdt.SDTInteractor_setSecondPoint(self._sdt_interactor, l)
		return self

	def dsp(self, double f0, double v0, double s0, double f1, double v1, double s1, np.ndarray[double, ndim=1, mode="c"] outs=None):
		return csdt.SDTInteractor_dsp(self._sdt_interactor, f0, v0, s0, f1, v1, s1, &outs[0])


cdef class Impact(Interactor):
	def __cinit__(self, **kwargs):
		self._sdt_interactor = csdt.SDTImpact_new()
		if self._sdt_interactor is NULL:
			raise MemoryError()

	def __init__(self, **kwargs):
		for k, v in kwargs.items():
			getattr(self, "set{}".format(k))(v)
	
	def __dealloc__(self):
		if self._sdt_interactor is not NULL:
			csdt.SDTImpact_free(self._sdt_interactor)

	def setStiffness(self, double f):
		csdt.SDTImpact_setStiffness(self._sdt_interactor, f)
		return self

	def setDissipation(self, double f):
		csdt.SDTImpact_setDissipation(self._sdt_interactor, f)
		return self

	def setShape(self, double f):
		csdt.SDTImpact_setShape(self._sdt_interactor, f)
		return self
